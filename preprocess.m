# Am format matricea X si vetorul t pentru functia learn

function [X t] = preprocess (dir_path, method , count_bins)

cats = strcat (dir_path,"cats/");
not_cats = strcat (dir_path,"not_cats/");
img_cats = getImgNames(cats);
img_not_cats = getImgNames(not_cats);

n = length (img_cats);
for i=1:n
	path = strcat(cats , img_cats(i,:));
	if method == "RGB"
		X(i,:) = rgbHistogram(path , count_bins);
	else 
		X(i,:) = hsvHistogram(path , count_bins); 
	end
	t(i) = 1;
end

m = length (img_not_cats);
for i=1:m
	path = strcat(not_cats , img_not_cats(i,:));
	if method == "RGB"
		X(i+n,:) = rgbHistogram(path , count_bins);
	else 
		X(i+n,:) = hsvHistogram(path , count_bins); 
	end
	t(i+n) = -1;
end

endfunction
