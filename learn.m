# Folosind functiile Gram_Schmidt (luata de pe cs curs)
# si invers (implementata de mine) voi calcula w;

function [w] = learn (X , t)

[n m] = size(X);
aux(1:n,1) = 1;
X = [ X aux ];

A = X'*X;

[Q R]=Gram_Schmidt_new(A);

# w = A^-1 * X' * t;
# A = QR => A^-1 = Q^-1 * R^-1 = Q' * R^-1
#                   Q^-1 = Q' 
# R fiind o matrice triunghiulara , folosim
# functia invers (R')' pentru a o calcula 

val = 105;
Q *= val; 
Q = round(Q); #Ceva corectare de eroare , nu stiu daca e de la PC-ul meu
Q /= val;

w = Q'*invers(R')'*X'*t';

endfunction