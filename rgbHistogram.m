# In aceasta functie luam map-ul pozei , il trecem intr-un vector 
# liniar si aplicam in acelasi timp si numararea valorilor ,
# cat si restrnagerea intervalului la count_bins diviziuni.


function [result] = rgbHistogram (file_path, count_bins)

map = double(imread (file_path));
[n m p] = size(map);
map = [ reshape(map(:,:,1),1,n*m) reshape(map(:,:,2),1,n*m) reshape(map(:,:,3),1,n*m) ];

aux = 256/count_bins;
for k=1:3
  for i=0:count_bins-1 
    result((k-1)*count_bins + i+1) = sum(map((k-1)*n*m+1:k*n*m) >= i*aux & map((k-1)*n*m+1:k*n*m) < (i+1)*aux);
  end
end

endfunction