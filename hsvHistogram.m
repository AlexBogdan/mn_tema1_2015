#Implementarea algoritmului din enunt + vectorizari
#
#Observatie : Am incercat sa folosesc interval 
#de forma celui de la task1 in count_bins diviziuni
#dar nu-mi corespunde outputul cu cel din ref , asa 
#ca am folosit acea constanta pentru precizie
#(Gasita dupa zeci de incearcari)

function [result] = hsvHistogram (file_path, count_bins)

result = imread(file_path);

R = double(result(:,:,1)); R = double(R/255);
G = double(result(:,:,2)); G = double(G/255);
B = double(result(:,:,3)); B = double(B/255);
[n m] = size(R); R = reshape(R,1,n*m); G = reshape(G,1,n*m); B = reshape(B,1,n*m);

Cmax = max(max(R,G),B);
Cmin = min(min(R,G),B);
V = Cmax;
delta = Cmax - Cmin;

S(Cmax ~= 0) = double(delta(Cmax ~= 0)./Cmax(Cmax ~= 0));
H(delta ~= 0 & Cmax == R) = mod((G(delta ~= 0 & Cmax == R)-B(delta ~= 0 & Cmax == R))./delta(delta ~= 0 & Cmax == R),6);
H(delta ~= 0 & Cmax == G) = (B(delta ~= 0 & Cmax == G)-R(delta ~= 0 & Cmax == G))./delta(delta ~= 0 & Cmax == G) + 2;
H(delta ~= 0 & Cmax == B) = (R(delta ~= 0 & Cmax == B)-G(delta ~= 0 & Cmax == B))./delta(delta ~= 0 & Cmax == B) + 4;
H /= 6;

aux = 258.555/(256*count_bins);
V*=100;
for i=1:count_bins
  H_aux(i) = sum(H >= (i-1)*aux & H < i*aux);
  S_aux(i) = sum(S >= (i-1)*aux & S < i*aux);
  V_aux(i) = sum(V>= (i-1)*100/count_bins & V < i*100/count_bins);
end

result = [ H_aux S_aux V_aux];
endfunction