#Algoritm pentru inversare matrice X inferior triunghiulara
#
# Daca avem o matrice superior triunghiulara vom apela
#        invers(A')'

function [result] = invers(A)

[n m] = size(A);
In  = eye(n);

A  = [A In];
for i = 1:n
    A(i,:) = A(i,:)/A(i,i);
    
    for ji = 1:i-1
        A(i,:) = A(i,:) - A(ji,:)*A(i,ji);
    end
end

result = A(:,n+1:end);
endfunction